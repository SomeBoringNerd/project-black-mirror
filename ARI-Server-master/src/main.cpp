#define ALLOW_DATABASE 1

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/wait.h>
#include <stdint.h>
#include <unistd.h>

#if ALLOW_DATABASE
#include <sqlite3.h>
#endif

#include <string>
#include <cstdint>
#include <regex>

#include "logger.hpp"
#include "camControl.hpp"
#include "imgUtil.hpp"
#include "server.hpp"
#include "dataBase.hpp"

#include "mini/ini.h"

/********************************************************************

                                todos

                                notes

                                 N/A

*********************************************************************/

#define DEBUG_MODE 0
#define ACTION_PING_CAMERA 1
#define ACTION_GET_IMAGE_FROM_CAMERA 0

// camera's ip
std::string ips[2] = {"192.168.1.2", "192.168.0.4"};

pthread_t camUn, camDeux, exit_listener;

int doit_exit = 1;

int respond[2] = {0, 0};
int ready[2] = {0, 0};
unsigned int iLoop[2] = {0, 0};
/**
 * Execution loop
 *
 * @param data : the camera's id as a void pointer
 * @return nothing I hope
 */
void *loop(void *data)
{
    int _ip = (intptr_t)data;
    int old_response = 0;
    ready[_ip] = 1;

    while (doit_exit)
    {
        // cheap and easy sync system
        if (ready[_ip ? 0 : 1])
        {
            ready[_ip] = 0;
            // check if the camera is online
            respond[_ip] = execute(ips[_ip], ACTION_PING_CAMERA);

            if (!respond[_ip])
            {
                if (old_response != respond[_ip])
                {
                    LogWarning("Camera with ip " + ips[_ip] + " is offline");
                    send("WARNING_EXCEPTION_NO_CAM_FOUND");
                }
            }
            else
            {
                if (old_response != respond[_ip])
                {
                    Log("Camera with ip " + ips[_ip] + " is back online");
                }

                if (!execute(ips[_ip], ACTION_GET_IMAGE_FROM_CAMERA))
                {
                    send("ERROR_COULDNT_GET_IMAGE");
                    LogError("Couldnt fetch an image from " + ips[_ip] + " (offline ?)");
                }
                else
                {
                    std::string plaque = getPlaque(ips[_ip] + ".jpg");

                    if (regex_match(plaque, std::regex("\p{Lu}{2}-\d{3}-\p{Lu}{2}")))
                    {
                        Log(plaque);
#if ALLOW_DATABASE
                        if (fetchDatabase(plaque))
                        {
                            Log("The car id is valid");
                        }
                        else
                        {
                            LogError("The car id is valid");
                        }
                    }
#endif
                }
            }

            old_response = respond[_ip];
            iLoop[_ip]++;

            if (!doit_exit)
            {
                Log("A thread was exited (in an intended way)");
                pthread_exit(0);
            }
            ready[_ip] = 1;
        }
    }

    // we shouldnt ever reach this code
    LogWarning("A thread was exited (not in an intended way)");
    return 0;
}

void initConfig()
{
    std::cout << "Reading config file" << std::endl;

    mINI::INIFile file("config.ini");
    mINI::INIStructure ini;
    file.read(ini);

    ips[0] = ini["devices"]["cam1"];
    ips[1] = ini["devices"]["cam2"];

    port = stoi(ini["wsserver"]["ws_port"]);

    std::string ip_serveur = (ini["database"]["ip"]);
    std::string base_port = (ini["database"]["port"]);

    Log("IP first cam : " + ips[0]);
    Log("IP second cam : " + ips[1]);
    Log("websocket port : " + std::to_string(port));
    std::cout << std::endl;
    Log("server's ip : " + ip_serveur);
    Log("Database's port : " + base_port);
}
/**
 * bunch of tests
 */
void UnitTestings()
{
    Log("[TEST] TEST SERIES FOR THE SERVER SOFTWARE");
    std::cout << std::endl;
    Log("[TEST] This test battery uses a predefined image to debug the program.");
    Log("[TEST] If you see this message in production, set DEBUG_MODE to 0");
    std::cout << std::endl;
    Log("[TEST] Chosen license plate for the test: GD-144-QH");
    Log("[TEST] File name: 2.jpg");

    std::string plate = getPlate("2.jpg");
    std::string plate2 = "GD-144-QH";

    Log("[TEST] Detected text: " + plate + "|");

    if (plate.find(plate2) != std::string::npos)
    {
        Log("[TEST] The test.jpg image contains the chosen license plate for the tests");
        std::cout << std::endl;
        if (fetchDatabase(plate))
        {
            Log("[TEST] The plate was found in the database");
            Log("[TEST] All tests have passed!");
        }
        else
        {
            LogError("[TEST] The plate was not found in the database");
        }
    }
    else
    {
        LogError("[TEST] The 3.jpg image does not contain the plate, it is an error.");
    }
}

int main()
{
   // During debugging, I noticed that WSserver needs to be executed as root
    // but it prevents the logger directory from being deleted
    // @TODO: Make sure the logs directory does not belong to root
    if (getuid())
    {
        printf("\033[0;31m");
        printf(" [ERROR] THIS SOFTWARE MUST BE RUN AS ROOT");
        printf("\n\033[0;0m");
        return 1;
    }

    initLogger("ServerSoftware");
    initConfig();

    doit_exit = 1;

    // Simple system to check if the cameras are online

#if !DEBUG_MODE

    respond[0] = execute(ips[0], ACTION_PING_CAMERA);
    respond[1] = execute(ips[1], ACTION_PING_CAMERA);

    int t1 = pthread_create(&camUn, NULL, loop, (void *)0);
    if (t1)
    {

        Log("Thread 1 could not be created");
        return 1;
    }
    else
    {
        Log("Thread cam 1 has been created");
    }

    int t2 = pthread_create(&camDeux, NULL, loop, (void *)1);
    if (t2)
    {

        Log("Thread 2 could not be created");
        return 1;
    }
    else
    {
        Log("Thread cam 2 has been created");
    }

    initWebSocket();

#endif
#if ALLOW_DATABASE
    initDatabase();
#endif
    char e;
#if DEBUG_MODE

    UnitTestings();
    return 0;
#endif

    scanf("%c", &e);

    doit_exit = 0;

    return 0;
}
