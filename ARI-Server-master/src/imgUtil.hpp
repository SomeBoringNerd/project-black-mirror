#pragma once

#include <stdio.h>
#include <string>
#include <iostream>
#include <stdlib.h>

#include <opencv4/opencv2/core.hpp>
#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/imgcodecs.hpp>
#include <opencv4/opencv2/highgui.hpp>
#include <opencv4/opencv2/opencv.hpp>
#include <tesseract/baseapi.h>
#include <iostream>

#include "logger.hpp"

/**
 * Formats the text so that only the plate text is returned and nothing else
 * @param _plaque input text
 * @return formatted text
 */
std::string getProperName(std::string _plaque)
{
    char *plaque_chr = new char[_plaque.length() + 1];

    const int length = _plaque.length();
    strcpy(plaque_chr, _plaque.c_str());

    for (int i = 0; i < length; i++)
    {
        // Enforce the format AA-123-BB
        // @TODO: Use regex to allow plates in the old format
        if (i < 2 || i > 6)
        {
            if (plaque_chr[i] == '0')
            {
                plaque_chr[i] = 'Q';
            }
            if (plaque_chr[i] == '6')
            {
                plaque_chr[i] = 'G';
            }
        }

        if (i > 8)
        {
            plaque_chr[i] = ' ';
        }

    }
    std::string properPlate = plaque_chr;
    return properPlate;
}

/**
 * Detects the location where a license plate may exist
 * @param filename image file name with extension (relative path)
 * @return the text on the plate after passing through <code>getProperName()</code>
 * @note this code is a translation of the Python script found here: https://shorturl.at/afjJO (gamingdeputy.com)
*/

std::string getPlaque(std::string filename)
{
    // Read the image
    cv::Mat original_image = cv::imread(filename);

    // Copy the image in grayscale
    cv::Mat gray_image;
    cvtColor(original_image, gray_image, cv::COLOR_BGR2GRAY);

    // Read the edges of the image
    cv::Mat edged_image;
    Canny(gray_image, edged_image, 30, 200);

    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;

    // Find contours in the image, store the points in 'contours' and the order in 'hierarchy'
    findContours(edged_image.clone(), contours, hierarchy, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
    cv::Mat img1 = original_image.clone();

    cv::imwrite("edge" + filename + ".png", edged_image);

    // Draw the contours on a copy of the original image
    drawContours(img1, contours, -1, cv::Scalar(0, 255, 0), 3);

    // Sort the contours and keep only 30 elements
    sort(contours.begin(), contours.end(), [](std::vector<cv::Point> a, std::vector<cv::Point>> b)
    { return contourArea(a) > cv::contourArea(b); });
    contours.resize(30);

    std::vector<cv::Point> screenCnt;
    cv::Mat img2 = original_image.clone();

    drawContours(img2, contours, -1, cv::Scalar(0, 255, 0), 3);

    // Write the image to disk
    cv::imwrite("contours" + filename + ".png", img2);

    int count = 0;
    int idx = 7;
    int i = 0;

    // Loop through the points
    // Set the 4 most likely points to resemble a separate rectangle
    // Then crop the image to keep only the presumed plate
    for (std::vector<cv::Point> c : contours)
    {
        double contour_perimeter = arcLength(c, true);
        std::vector<cv::Point> approx;
        approxPolyDP(c, approx, 0.018 * contour_perimeter, true);

        if (approx.size() == 4)
        {
            screenCnt = approx;
            cv::Rect rect = boundingRect(c);
            cv::Mat new_img = original_image(rect);
            cv::imwrite(filename + ".png", new_img);
            idx++;
            break;
        }
    }

    // Fix a stupid crash
    // If screenCnt (vector of points) is empty, attempting to draw contours using screenCnt as a reference causes a crash.
    // Returning an empty string if screenCnt is empty seems to be a working solution.
    if (screenCnt.empty())
    {
        return "";
    }
    else
    {
        drawContours(original_image, std::vector<std::vector<cv::Point>>{screenCnt}, -1, cv::Scalar(0, 255, 0), 3);
    }

    // Tesseract part
    std::string cropped_License_Plate = filename + ".png";
    tesseract::TessBaseAPI tess;
    tess.Init(NULL, "fra", tesseract::OEM_LSTM_ONLY);
    tess.SetPageSegMode(tesseract::PSM_SINGLE_BLOCK);

    tess.SetImage(cv::imread(cropped_License_Plate).data, cv::imread(cropped_License_Plate).cols, cv::imread(cropped_License_Plate).rows, 3, cv::imread(cropped_License_Plate).step);
    std::string text = tess.GetUTF8Text();
#if DEBUG_MODE
    Log("Text detected on the image : " + text);
#endif
    return getProperName(text);
}