#pragma once

#include "include/ws.h"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>

#include "logger.hpp"

ws_cli_conn_t *client;
int isClientLoggedIn = 0;

int port = 0;

/********************************************************************

        Base de donnée et autre joyeusetées

*********************************************************************/

/**
 * @brief check authentification.
 */
void Login(char *username, char *password)
{
}

/********************************************************************

                    websocket's events

*********************************************************************/
/**
 * @brief called on connexion
 * @param client : connexion
 */
void onopen(ws_cli_conn_t *_client)
{
    // if the client is already logged in, discard request
    if (client != NULL)
        return;

    client = _client;

    isClientLoggedIn = true;
    char *cli;
    cli = ws_getaddress(client);
    Log("Connection opened\n");
}

/**
 * @brief called on disconnection
 * @param client Client connection.
 */
void onclose(ws_cli_conn_t *_client)
{
    char *cli;
    isClientLoggedIn = false;
    client = NULL;
    cli = ws_getaddress(client);
    Log("Connection closed, addr\n");
}

/**
 * @brief callec on message received
 * @param client : Connexion
 * @param msg the message's content
 */
void onmessage(ws_cli_conn_t *_client, const unsigned char *msg, uint64_t msg_size, int type)
{
    char *cli, *con_ip;
    con_ip = ws_getaddress(_client);
    cli = ws_getaddress(client);

    if (cli != con_ip)
        return;
}
/**
 * @brief ws_sendframe_txt wrapper
 * @param message message to send
 */
void send(char *message)
{
    if (client != NULL || !isClientLoggedIn)
    {
        ws_sendframe_txt(client, message);
    }
    else
    {
        LogError("Tried to send a message but there's no clients");
    }
}

void initWebSocket()
{
    struct ws_events evs;
    evs.onopen = &onopen;
    evs.onclose = &onclose;
    evs.onmessage = &onmessage;

    ws_socket(&evs, port, 0, 1000);
}