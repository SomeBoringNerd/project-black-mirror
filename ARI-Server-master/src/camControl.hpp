#pragma once

#include <stdio.h>
#include <string>
#include <stdlib.h>

#include "logger.hpp"

/********************************************************************

                                todos

                                notes

        Ce fichier est lié au controle des caméras.

*********************************************************************/

/**
 * Ping an IP address to check if a camera is on the local network.
 *
 * @param ip: IP address of the device to ping
 * @param ping: determines what the function will do (1 = Ping, 0 = download image from the camera)
 * @return: Success of the operation (1 = success, 0 = failure)
 */
int execute(std::string ip, int ping)
{
    // Try to ping the provided IP address once
    std::string cmd;

    if (ping)
    {
        cmd = "ping -c 1 " + ip + " -W 1 -q > /dev/null 2>&1";
    }
    else
    {
        cmd = "ffmpeg -y -i rtsp://" + ip + ":554 -frames:v 1 " + ip + ".jpg > /dev/null 2>&1";
    }

    // Check the success of the command
    int status = system(cmd.c_str());

    if (-1 != status)
    {
        // Return the inverse of WEXITSTATUS because in C, 0 == false
        // By inverting the status, we return 1 if the ping receives a response, 0 if it times out
        int ping_ret = WEXITSTATUS(status);
        return !ping_ret;
    }
    else
    {
        return 0;
    }
}