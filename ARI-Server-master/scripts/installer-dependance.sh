if [ "$EUID" -ne 0 ]
  then echo "This script need to be run as root"
  exit
fi

# Update APT repos and install dependencies
apt update
apt install -y g++ wget unzip build-essential iputils-ping xterm ffmpeg libtesseract-dev libblis-pthread-dev

# we want cmake 3.26
wget https://github.com/Kitware/CMake/releases/download/v3.26.0-rc5/cmake-3.26.0-rc5-linux-x86_64.sh -O cmake-build.sh

chmod +x /cmake-build.sh
mkdir /opt/cmake
/bin/bash cmake-build.sh --prefix=/opt/cmake --skip-license
ln -s /opt/cmake/bin/cmake /usr/local/bin/cmake

# build OpenCV 4, take a while so grab yourself a coffee

wget -O opencv.zip https://github.com/opencv/opencv/archive/4.x.zip
unzip opencv.zip
mkdir -p build

cd build
cmake  ../opencv-4.x
cmake --build .

echo "The software is ready to be build"
echo "run the script \"compiler_serveur\""